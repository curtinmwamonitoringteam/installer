#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function seed_database_webbackend_init() {
	task_setup "seed_database_webbackend" "Seed webbackend database" "Seed the webbackend database" "install_packages clone_webbackend install_requirements_webbackend setup_database_webbackend setup_schema_webbackend"
}

function seed_database_webbackend_skip() {
	log_info "Skipping seeding of webbackend database"
}

function seed_database_webbackend_run() {
	log_info "Seeding webbackend database"

	try {
		cd ${WEBBACKEND_DIR}

		# Seed database
		python src/database/seed_db.py
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}