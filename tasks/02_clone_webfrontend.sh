#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function clone_webfrontend_init() {
	task_setup "clone_webfrontend" "Clone webfrontend" "Clone the webfrontend repository" "install_packages"
}

function clone_webfrontend_skip() {
	log_info "Skipping cloning of webfrontend"
}

function clone_webfrontend_run() {
	log_info "Cloning webfrontend"

	try {
		if [ -d ${WEBFRONTEND_DIR} ]; then
			log_important "Repository already exists! skipping."

			return ${E_SUCCESS}
		else
			log_info "Navigating to ${WEBFRONTEND_DIR}"
			cd ${WEBFRONTEND_CLONE_DIR}

			log_info "Cloning from ${WEBFRONTEND_GIT_URL}"
			git clone ${WEBFRONTEND_GIT_URL}

			log_info "Granting full permissions (777) to webfrontend repo"
			sudo chmod 777 ${WEBFRONTEND_DIR}
		fi
	} catch {
		log_error "Issue while cloning repo! Deleting repo."

		rm -rf ${WEBFRONTEND_DIR}

		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}