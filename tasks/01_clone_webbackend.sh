#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch
import ${INSTALLER_PATH}/lib/util/type

function clone_webbackend_init() {
	task_setup "clone_webbackend" "Clone webbackend" "Clone the webbackend repository" "install_packages"
}

function clone_webbackend_skip() {
	log_info "Skipping cloning of webbackend"
}

function clone_webbackend_run() {
	log_info "Cloning webbackend"

	try {
		if [ -d ${WEBBACKEND_DIR} ]; then
			log_important "Repository already exists! skipping."

			return ${E_SUCCESS}
		else
			log_info "Navigating to ${WEBBACKEND_DIR}"
			cd ${WEBBACKEND_CLONE_DIR}

			log_info "Cloning from ${WEBBACKEND_GIT_URL}"
			git clone ${WEBBACKEND_GIT_URL}

			log_info "Granting full permissions (777) to webbackend repo"
			sudo chmod 777 ${WEBBACKEND_DIR}
		fi
	} catch {
		log_error "Issue while cloning repo! Deleting repo."

		rm -rf ${WEBBACKEND_DIR}

		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}