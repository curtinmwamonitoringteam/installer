#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function setup_database_webbackend_init() {
	task_setup "setup_database_webbackend" "Setup database webbackend" "Setup the webbackend database connection" "install_packages clone_webbackend"
}

function setup_database_webbackend_skip() {
	log_info "Skipping setup of webbackend database"
}

function setup_database_webbackend_run() {
	log_info "Setting up webbackend database"

	# Ask for credentials
	string user=$(enter_variable "Please enter the database root username." "root")
	string password=$(enter_variable "Please enter the password (default none)." "")

	try {
		cd ${WEBBACKEND_DIR}

		log_info "Creating production database"
		echo "mysql -u '$user' '-p$password' < 'src/database/prod_database_setup.sql'"

		mysql -u "$user" "-p$password" < "src/database/prod_database_setup.sql"

		echo ""

		log_info "Creating test database"
		echo "mysql -u '$user' '-p$password' < 'src/database/test_database_setup.sql'"

		mysql -u "$user" "-p$password" < "src/database/test_database_setup.sql"
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}