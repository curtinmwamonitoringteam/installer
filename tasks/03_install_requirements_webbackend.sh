#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function install_requirements_webbackend_init() {
	task_setup "install_requirements_webbackend" "Install requirements webbackend" "Install the pip requirements file for webbackend" "install_packages clone_webbackend"
}

function install_requirements_webbackend_skip() {
	log_info "Skipping installation of webbackend requirements"
}

function install_requirements_webbackend_run() {
	log_info "Installing webbackend requirements"

	try {
		cd ${WEBBACKEND_DIR}

		sudo pip install -r requirements.txt
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}