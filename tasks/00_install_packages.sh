#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function install_packages_init() {
	task_setup "install_packages" "System Packages" "Install linux system packages"
}

function install_packages_skip() {
	log_info "Skipping system package installation"
}

function install_packages_run() {
	log_info "Installing system packages"

	try {
		xargs -a <(awk '! /^ *(#|$)/' "${INSTALLER_PATH}/packagelist.txt") -r -- sudo apt-get install || exit 255
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}