#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function setup_schema_webbackend_init() {
	task_setup "setup_schema_webbackend" "Setup webbackend database schema" "Setup the webbackend database schema" "install_packages clone_webbackend setup_database_webbackend"
}

function setup_schema_webbackend_skip() {
	log_info "Skipping setup of webbackend database schema"
}

function setup_schema_webbackend_run() {
	log_info "Setting up webbackend database schema"

	try {
		cd ${WEBBACKEND_DIR}

		# Schema setup
		python src/database/schema_setup.py
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}