#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch

function npm_install_webfrontend_init() {
	task_setup "npm_install_webfrontend" "Install webfrontend dependecies" "Install the webfrontend dependencies" "install_packages clone_webfrontend"
}

function npm_install_webfrontend_skip() {
	log_info "Skipping installation of webfrontend dependencies"
}

function npm_install_webfrontend_run() {
	log_info "Installing webfrontend dependencies"

	try {
		cd ${WEBFRONTEND_DIR}

		npm install
	} catch {
		return ${E_FAILURE}
	}

	return ${E_SUCCESS}
}