#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/class
import ${INSTALLER_PATH}/lib/util/tryCatch
import ${INSTALLER_PATH}/lib/util/namedParameters

class:Repository() {
	public string url
	public string path
	public string folder

	Repository.check_if_local_repo_exists() {
		try {
			if [ ! -d $(this path)/$(this folder) ]; then
				git clone $(this url)
				mv $(this folder) $(this path)
			fi
		} catch {
			log_error "Error while checking if $(this folder) repository exists / cloning"
			rm -rf $(this folder)

			return ${E_FAILURE}
		}

		return ${E_SUCCESS}
	}

	Repository.update_repo() {
		try {
			echo $PWD

			$(this check_if_local_repo_exists)

			cd $(this path)
			cd $(this folder)

			echo $PWD
			echo $ls

			# Get new tags from the remote
			$(git fetch --tags)

			# Get the current tag name
			currentTag=$(git describe --abbrev=0 --tags)

			# Will get the latest tag
			latestTag=$(git for-each-ref refs/tags --sort=-taggerdate --format='%(refname:short)' --count=1)

			echo ""
			log_info "Local Version:  $currentTag"
			log_info "Remote Version: $latestTag"
			echo ""

			# Check if latest is newer than current
			#if semver_gt $latestTag $currentTag; then
				log_info "Shutting down $(this folder) service..."


				# Checkout the latest tag
				log_info "Rebasing onto latest tag..."
				$(git rebase master)
			#fi
		} catch {
			cd $INSTALLER_PATH
		}

		cd $INSTALLER_PATH
	}
}

# Initialize the class
Type::Initialize Repository

function update() {
	# Create an object called 'Webbackend' of type Repository
	Repository Webbackend

	$var:Webbackend url = ${WEBBACKEND_GIT_URL}
	$var:Webbackend path = ${WEBBACKEND_PATH}
	$var:Webbackend folder = ${WEBBACKEND_DIR}

	# Create an object called 'Webfrontend' of type Repository
	Repository Webfrontend

	$var:Webfrontend url = ${WEBFRONTEND_GIT_URL}
	$var:Webfrontend path = ${WEBFRONTEND_PATH}
	$var:Webfrontend folder = ${WEBFRONTEND_DIR}

	# Update each repo
	$var:Webbackend update_repo
	$var:Webfrontend update_repo
}