#!/usr/bin/env bash

source "$( cd "${BASH_SOURCE[0]%/*}" && pwd )/../lib/oo-bootstrap.sh"

import ${INSTALLER_PATH}/lib/util/tryCatch
import ${INSTALLER_PATH}/lib/util/type
import ${INSTALLER_PATH}/lib/util/log

function process_pickle_dir() {
	string path=$(enter_variable "Please enter full path to pickle dir." "/home/mwa/dipoletests")

	cd $path
	integer total=$(find . -type f -name "*.pickle*" | wc -l)

	if [ "$total" -eq "0" ]; then
		echo "Cannot find any .pickle files in the specified directory"
		return ${E_FAILURE}
	fi

	tempFileCount=/tmp/count.tmp
	echo 0 > $tempFileCount

	tempFileSuccess=/tmp/success.tmp
	echo 0 > $tempFileSuccess

	tempFileFail=/tmp/fail.tmp
	echo 0 > $tempFileFail

	echo "=========================================================" >> process_pickle_dir.status


	for pickleFile in *.pickle*; do
		count=$[$(cat $tempFileCount) + 1]
		echo "" >> process_pickle_dir.status

		try {
			success=$[$(cat $tempFileSuccess) + 1]			

			echo "curl -X POST -F 'file=@$pickleFile' --url http://127.0.0.1:5000/upload"
			curl -X POST -F 'file=@$pickleFile' --url http://127.0.0.1:5000/upload 2>&1 | tee process_pickle_dir.status

			echo "rm -fv $pickleFile"
			rm -fv $pickleFile 2>&1 | tee process_pickle_dir.status

			echo "" >> process_pickle_dir.status
			echo "[$count/$total] | SUCCESS | $pickleFile"
			echo "[$count/$total] | SUCCESS | $pickleFile" >> process_pickle_dir.status
			
			echo $success > $tempFileSuccess
		} catch {
			fail=$[$(cat $tempFileFail) + 1]

			echo "" >> process_pickle_dir.status
			echo "[$count/$total] | FAILURE | $pickleFile"
			echo "[$count/$total] | FAILURE | $pickleFile" >> process_pickle_dir.status

			echo $fail > $tempFileFail
		}

		echo $count > $tempFileCount
	done

	echo ""
	echo "" >> process_pickle_dir.status
	echo "========================================================="
	echo "=========================================================" >> process_pickle_dir.status
	echo "Total:   $total"
	echo "Total:   $total" >> process_pickle_dir.status

	integer	success=$[$(cat $tempFileSuccess)]
	integer fail=$[$(cat $tempFileFail)]

	if [ "$success" -gt "0" ]; then
		echo "Success: $(UI.Color.Green)$success$(UI.Color.Default)"
		echo "Success: $(UI.Color.Green)$success$(UI.Color.Default)" >> process_pickle_dir.status
	else
		echo "Success: $success"
		echo "Success: $success" >> process_pickle_dir.status
	fi

	if [ "$fail" -gt "0" ]; then
		echo "Fail:    $(UI.Color.Red)$fail$(UI.Color.Default)"
		echo "Fail:    $(UI.Color.Red)$fail$(UI.Color.Default)" >> process_pickle_dir.status
	else
		echo "Fail:    $fail"
		echo "Fail:    $fail" >> process_pickle_dir.status
	fi

	echo ""
	echo "" >> process_pickle_dir.status

	unlink $tempFileCount
	unlink $tempFileSuccess
	unlink $tempFileFail

	return ${E_SUCCESS}
}
