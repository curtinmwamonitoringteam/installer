https://github.com/FlavourSys/bash-installer-framework
https://github.com/niieani/bash-oo-framework
https://julienrenaux.fr/2013/10/04/how-to-automatically-checkout-the-latest-tag-of-a-git-repository/

# Install

1. Check for sudo
    - If no sudo then fail
2. Install system packages
3. Clone webbackend and webfrontend
4. Install webbackend
5. Install webfrontend

## Install Webbackend

1. Virtual env
    - Install
    - Activate
2. Install requirements
3. Prod and test database setup
4. Schema Setup
5. Seed DB
6. Run tests

## Install Webfrontend

1. Install npm

# Update

1. Check repos exist
2. Checkout latest tag for webbackend
3. Checkout latest tag for webfrontend

# Start (Run)

1. Check if either project is not running
1. Start the non running project(s)

# Stop

1. Check if either project is running
2. Stop the running project(s)

# Restart

1. Stop both projects
2. Start both projects

# Version

1. Fetch tags for all projects (installer, webbackend, webfrontend)
2. For each project:
    - Print current local version tag
    - If new version, print remote version tag

# Help

1. Display skel files for each command
