# User configuration.
#
# This file should contain the product-specific configuration.
# For example, one may overwrite default global configuration
# values such as ROOT_ONLY.
# Plus, one should define the three greeter functions 'welcome',
# 'installation_complete', and 'installation_incomplete'.

# Set to 1 to enforce root installations.
#ROOT_ONLY=0

# Overwrite to disable the initial touch-all-files.
#INITIAL_TOUCH_ALL=1

# Overwrite to disable task dependency checking.
#TASK_DEPENDENCY_CHECKING=1

# Overwrite default utils & tasks directories.
#UTILS_DIR=${INSTALLER_PATH}/data/utils
#TASKS_DIR=${INSTALLER_PATH}/data/tasks

# Overwrite default logs directory.
LOGS_DIR="${INSTALLER_PATH}/logs"

# Webbackend project location (relative to tasks dir if relative at all)
#WEBBACKEND_GIT_URL="git@bitbucket.org:curtinmwamonitoringteam/webbackend.git"
WEBBACKEND_GIT_URL="https://ScottDay@bitbucket.org/curtinmwamonitoringteam/webbackend.git"

WEBBACKEND_CLONE_DIR="/home/mwa"
#WEBBACKEND_CLONE_DIR="/home/scott/Documents/Uni"

WEBBACKEND_DIR="${WEBBACKEND_CLONE_DIR}/webbackend"

# Webfrontend project location (relative to tasks dir if relative at all)
#WEBFRONTEND_GIT_URL="git@bitbucket.org:curtinmwamonitoringteam/webfrontend.git"
WEBFRONTEND_GIT_URL="https://ScottDay@bitbucket.org/curtinmwamonitoringteam/webfrontend.git"

WEBFRONTEND_CLONE_DIR="/home/mwa"
#WEBFRONTEND_CLONE_DIR="/home/scott/Documents/Uni"

WEBFRONTEND_DIR="${WEBFRONTEND_CLONE_DIR}/webfrontend"

# Task status dir
TASK_STATUS_DIR=${INSTALLER_PATH}/status

# Overwrite default log-to-stdout config.
#LOG_STDOUT=( "ERROR" "IMPORTANT" "WARNING" "INFO" "SKIP" "START" "FINISH" )

function welcome() {
  echo -e "\e[00;32mICRAR MWA Monitor Installer and Maintenance Tool!\e[00m"
}

function installation_complete() {
  echo -e "\e[00;32mInstallation Complete!\e[00m"

  # If you want the install script to terminate automatically:
  #exit 0
}

function installation_incomplete() {
  echo -e "\e[00;31mInstallation Incomplete!\e[00m"
}

#function main_menu_prompt() {
#  echo "What ye want?"
#}

#function task_menu_prompt() {
#  echo "TAAASK?"
#}

#function skip_menu_prompt() {
#  echo "SKIPP?"
#}